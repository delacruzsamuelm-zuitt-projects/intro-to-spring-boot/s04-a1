package com.delacruz.b142.s02.s02app.controllers;

import com.delacruz.b142.s02.s02app.models.User;
import com.delacruz.b142.s02.s02app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;

    // Creating a new user
    @RequestMapping(value="/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User newUser) {
        userService.createUser(newUser);
        return new ResponseEntity<>("New user was successfully created.", HttpStatus.CREATED);
    }

    // Retrieve all users
    @RequestMapping(value="/users", method=RequestMethod.GET)
    public ResponseEntity<Object> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    // Update an existing user
    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody User updatedUser) {
        userService.updateUser(id, updatedUser);
        return new ResponseEntity<>("User was successfully updated!", HttpStatus.OK);
    }

    // Delete an existing user
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return new ResponseEntity<>("User was successfully deleted!", HttpStatus.OK);
    }

}

