package com.delacruz.b142.s02.s02app.services;

import com.delacruz.b142.s02.s02app.models.Post;

public interface PostService {
    void createPost(Post newPost, Long userId);
    void updatePost(Long userId, Long postId, Post updatedPost);
    void deletePost(Long id);
    Iterable<Post> getPosts();
    Iterable<Post> getMyPosts(Long userID);
}

